# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf
import numpy as np
import PIL.Image
import time
import functools

import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams['figure.figsize'] = (12,12)
mpl.rcParams['axes.grid'] = False

import tensorflow_hub as hub


tf.compat.v1.enable_eager_execution()

alt_content_path = tf.keras.utils.get_file('YellowLabradorLooking_new.jpg', 'https://storage.googleapis.com/download.tensorflow.org/example_images/YellowLabradorLooking_new.jpg')
alt_style_path = tf.keras.utils.get_file('kandinsky5.jpg','https://storage.googleapis.com/download.tensorflow.org/example_images/Vassily_Kandinsky%2C_1913_-_Composition_7.jpg')

content_path = tf.keras.utils.get_file('a-white-bed-frame-with-gray-blankets-on-top-in-a-bedroom-wit-484127c418a27e9cfd1088a82cd1c351.jpg','https://www.ikea.com/images/a-white-bed-frame-with-gray-blankets-on-top-in-a-bedroom-wit-484127c418a27e9cfd1088a82cd1c351.jpg?f=s')
style_path = tf.keras.utils.get_file('VanGogh--Bedroom--1889--MuseedOrsayParis--1400.jpg','https://archive.artic.edu/van-gogh-bedrooms/VanGogh--Bedroom--1889--MuseedOrsayParis--1400.jpg')
print(tf.__version__)


def tensor2image(tensor):
    tensor = tensor * 255
    tensor = np.array(tensor, dtype=np.uint8)
    if np.ndim(tensor) > 3:
        assert tensor.shape [0] == 1
    tensor = tensor [0]
    return PIL.Image.fromarray(tensor)


def disp_img (image, title=None): 
    if len(image.shape):
        image = tf.squeeze(image, axis = 0)
        
    plt.imshow(image)
    if title:
        plt.title(title)
        
def load_img(path_to_img):
  max_dim = 512
  img = tf.io.read_file(path_to_img)
  img = tf.image.decode_image(img, channels=3)
  img = tf.image.convert_image_dtype(img, tf.float32)

  shape = tf.cast(tf.shape(img)[:-1], tf.float32)
  long_dim = max(shape)
  scale = max_dim / long_dim

  new_shape = tf.cast(shape * scale, tf.int32)

  img = tf.image.resize(img, new_shape)
  img = img[tf.newaxis, :]
  return img

contimg = load_img(content_path)
stylimg = load_img(style_path)

plt.subplot(1, 2, 1)
disp_img (contimg, 'content image')

plt.subplot(1, 2, 2)
disp_img (stylimg, 'style image')

hub_module = hub.load('https://tfhub.dev/google/magenta/arbitrary-image-stylization-v1-256/1')

out_image = hub_module(tf.constant(contimg), tf.constant(stylimg))[0]

tensor2image(out_image)

vgg = tf.keras.applications.VGG19(include_top=False, weights='imagenet')

def vgglayers (layer_names): 
    #VGG19 model
    vgg = tf.keras.applications.VGG19(include_top=False, weights='imagenet')
    vgg.trainable = False
    
    outputs = [vgg.get_layer(name).output for name in layer_names]
    fmodel = tf.keras.Model ([vgg.input], outputs)
    return fmodel

def gram_matrix(input_tensor):
  result = tf.linalg.einsum('bijc,bijd->bcd', input_tensor, input_tensor)
  input_shape = tf.shape(input_tensor)
  num_locations = tf.cast(input_shape[1]*input_shape[2], tf.float32)
  return result/(num_locations)

#content layers
contlayers = ['block5_conv2'] 

# assign style layers
styl_layers = ['block1_conv1',
                'block2_conv1',
                'block3_conv1', 
                'block4_conv1', 
                'block5_conv1']

num_content_layers = len(contlayers)
num_style_layers = len(styl_layers)

style_extractor = vgglayers(styl_layers)
style_outputs = style_extractor(stylimg*255)

extractor = StyleContentModel(style_layers, content_layers)

results = extractor(tf.constant(content_image))

style_results = results['style']

class StyleContentModel(tf.keras.models.Model):
  def __init__(self, styl_layers, content_layers):
    super(StyleContentModel, self).__init__()
    self.vgg =  vgglayers(styl_layers + contlayers)
    self.style_layers = styl_layers
    self.content_layers = contlayers
    self.num_style_layers = len(styl_layers)
    self.vgg.trainable = False

  def call(self, inputs):
    "Expects float input in [0,1]"
    inputs = inputs*255.0
    preprocessed_input = tf.keras.applications.vgg19.preprocess_input(inputs)
    outputs = self.vgg(preprocessed_input)
    style_outputs, content_outputs = (outputs[:self.num_style_layers], 
                                      outputs[self.num_style_layers:])

    style_outputs = [gram_matrix(style_output)
                     for style_output in style_outputs]

    content_dict = {content_name:value 
                    for content_name, value 
                    in zip(self.content_layers, content_outputs)}

    style_dict = {style_name:value
                  for style_name, value
                  in zip(self.style_layers, style_outputs)}

    return {'content':content_dict, 'style':style_dict}

style_targets = extractor(stylimg)['style']
content_targets = extractor(contimg)['content']





